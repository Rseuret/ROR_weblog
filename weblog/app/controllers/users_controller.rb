class UsersController < ApplicationController
  before_action :load_user, only:[:show,:update,:edit,:destroy]
  before_action :authenticate_user!

  def index
    @users = User.all
  end

  def create
    @u = User.new(user_params)
    if @u.save
        redirect_to root_path
    else
        render "new"
    end
  end

  def new
    @u=User.new
  end

  def update
    if @u.update_attributes(user_params)&& (@u == current_user)
        redirect_to user_path(@u)
    else
        render "404"
    end
  end

  def destroy
    @u.destroy
    redirect_to root_path
  end

  def user_params
    params.require(:user).permit(:pseudo,:firstname,:lastname)
  end

  def load_user
    @u=User.find(params[:id])
  end

end
