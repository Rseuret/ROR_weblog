class PostsController < ApplicationController
    before_action :load_post, only:[:show,:update,:edit,:destroy,:load_user]
    before_action :load_user

  def index
    @p = Post.order('created_at DESC')

  end

  def create
    @p = Post.new(post_params)
    @p.user=@u
    if @p.save
        redirect_to post_path(@p)
    else
        render "new"
    end
  end

  def new
    @p=Post.new

  end

  def update
    if @p.update_attributes(post_params) && @p.user == current_user
        redirect_to post_path(@p)
    else
        render "404"
    end
   end

  def destroy
    @p.destroy
    redirect_to posts_path
  end

  def post_params
    params.require(:post).permit(:titre,:chapeau,:vignette,:user_id)
  end

  def load_post
    @p=Post.find(params[:id])
  end
  def load_user
    @u=current_user #essayez de load user courant
  end
end
