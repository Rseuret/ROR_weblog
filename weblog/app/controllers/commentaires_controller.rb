class CommentairesController < ApplicationController
  before_action :load_post 

    def index
      @commentaire = @p.commentaires
    end

    def create
      @c = Commentaire.new(comment_params)
      @c.user = current_user
      @c.post = @p
      if @c.save
          redirect_to post_path(@p)
      else
          render "new"
      end
    end

    def new
      @c = Commentaire.new
    end

  #  def update
  #    if @c.update_attributes(comment_params)
  #        redirect_to user_photo_path(@u,@p)
  #    else
  #        render "pas po"
  #    end
  #  end

    def destroy
      @c.destroy
      redirect_to post_path(@p)
    end

    def comment_params
      params.require(:commentaire).permit(:name,:user_id,:post_id)
    end

    def load_comment
      Commentaire.find(params[:id])
    end

    def load_user
      @u=current_user
    end

    def load_post
      @p=Post.find(params[:post_id])
    end

end
