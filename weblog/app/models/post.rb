class Post < ActiveRecord::Base
  has_many :commentaires
  belongs_to :user
  mount_uploader :vignette, PostUploader
end
