class CreateCommentaires < ActiveRecord::Migration
  def change
    create_table :commentaires do |t|
      t.text "text"
      t.integer "user_id"
      t.integer "posts_id"
      t.timestamps
    end
  end
end
