class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string "titre"
      t.text "chapeau"
      t.integer "user_id"
      t.string "vignette"
      t.timestamps
    end
  end
end
